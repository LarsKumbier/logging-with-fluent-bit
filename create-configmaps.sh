#!/bin/bash

if kubectl get configmap fluent-bit-config &> /dev/null; then
    kubectl delete configmap fluent-bit-config
fi

if kubectl get configmap fluentd-config &> /dev/null; then
    kubectl delete configmap fluentd-config
fi

kubectl create configmap fluent-bit-config --from-file config/fluent-bit.conf --from-file config/fluent-bit-parsers.conf
kubectl create configmap fluentd-config --from-file config/fluent.conf

