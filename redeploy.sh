#!/bin/bash

set -e

pushd fluentd/
docker build --tag larskumbier/fluentd:test .
docker push larskumbier/fluentd:test
popd

./create-configmaps.sh

kubectl delete service fluentd
kubectl delete daemonset fluent-bit
kubectl delete deploy fluentd
if [ ! -z "$REDEPLOY_DEPS" ]; then
    echo "=== redeploying dependencies ==="
    kubectl delete service graylog
    kubectl delete deploy graylog
    kubectl delete service mongodb
    kubectl delete deploy mongodb
    kubectl delete service elasticsearch
    kubectl delete deploy elasticsearch
    kubectl create -f test-dependencies/elasticsearch.deployment.yml
    kubectl create -f test-dependencies/elasticsearch.service.yml
    kubectl create -f test-dependencies/mongodb.deployment.yml
    kubectl create -f test-dependencies/mongodb.service.yml
    kubectl create -f test-dependencies/graylog.deployment.yml
    kubectl create -f test-dependencies/graylog.service.yml
else
    echo "=== ENV:REDEPLOY_DEPS not set - skipping redeploying dependencies ==="
fi
kubectl create -f fluentd/fluentd.deployment.yml
kubectl create -f fluentd/fluentd.service.yml
kubectl create -f fluent-bit/fluent-bit.daemonset.yml
